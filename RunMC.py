
###################################################
# This file runs a Monte Carlo simulation
# using the vehicle information from Vehicle Inputs
###################################################



import numpy as np
import VehicleSequences as seq
import VehicleInputs as inp
import matplotlib.pyplot as plt
from scipy import stats
import copy



# Define MC Information (number of cases and success ratio)
nCases =1000
pctSuccess = 0.99
caseCutoffLow = int(np.ceil((1-pctSuccess)*nCases))
caseCutoffHigh = int(np.ceil(pctSuccess*nCases))
listStage1 = []
listStage2 = []

# Initialize storage vectors
mStg1PropOxVec      = np.ones((nCases, 1))*np.NaN
mStg1PropFuelVec    = np.ones((nCases, 1))*np.NaN
mStg1ExcessVec      = np.ones((nCases, 1))*np.NaN

mStg2PropOxVec      = np.ones((nCases, 1))*np.NaN
mStg2PropFuelVec    = np.ones((nCases, 1))*np.NaN
mStg2ExcessVec      = np.ones((nCases, 1))*np.NaN

# Generate random draws 
multStg1Chill = np.random.default_rng().normal(0.75, 0.1, nCases)
multStg1PropRCS = np.random.default_rng().normal(0.75, 0.1, nCases)
multStg1Boiloff = np.random.default_rng().uniform(0.9, 1, nCases)
multStg1Leakage = np.random.default_rng().uniform(0.9, 1, nCases)
multStg1DV      = np.random.default_rng().uniform(0.8, 1, nCases)
multStg1Isp     = np.random.default_rng().uniform(1, 1.00888, nCases)


multStg2Chill = np.random.default_rng().normal(0.7, 0.1, nCases)
multStg2PropRCS = np.random.default_rng().normal(0.7, 0.1, nCases)
multStg2Boiloff = np.random.default_rng().uniform(0.8, 1, nCases)
multStg2Leakage = np.random.default_rng().uniform(0.9, 1, nCases)
multStg2DV      = np.random.default_rng().uniform(0.8, 1, nCases)
multStg2Isp     = np.random.default_rng().uniform(1, 1.00888, nCases)


InputsStg1 = copy.deepcopy(inp.InputsStg1)
InputsStg2 = copy.deepcopy(inp.InputsStg2)


InputsNomStg1 = copy.deepcopy(inp.InputsStg1)
InputsNomStg2 = copy.deepcopy(inp.InputsStg2)


# Loop through a number of runs
for ii in range(nCases):
    if ii ==0:
        # The first case is always a nominal
        print('Running Nominal')
    else:
        # Otherwise, modify the inputs based on the multiplier draws
        InputsStg1["mPropChillOx"]   = InputsNomStg1["mPropChillOx"]*multStg1Chill[ii]
        InputsStg1["mPropChillFuel"] = InputsNomStg1["mPropChillFuel"]*multStg1Chill[ii]
        
        InputsStg1["mdotPropRCSOx"]   = InputsNomStg1["mdotPropRCSOx"]*multStg1PropRCS[ii]
        InputsStg1["mdotPropRCSFuel"] = InputsNomStg1["mdotPropRCSFuel"]*multStg1PropRCS[ii]
        
        InputsStg1["mdotBoiloffOx"]   = InputsNomStg1["mdotBoiloffOx"]*multStg1Boiloff[ii]
        InputsStg1["mdotBoiloffFuel"] = InputsNomStg1["mdotBoiloffFuel"]*multStg1Boiloff[ii]
        
        InputsStg1["mdotLeakageOx"]   = InputsNomStg1["mdotLeakageOx"]*multStg1Leakage[ii]
        InputsStg1["mdotLeakageFuel"] = InputsNomStg1["mdotLeakageFuel"]*multStg1Leakage[ii]
        
        InputsStg1["pctDVReserveBurned"]   = InputsNomStg1["pctDVReserveBurned"]*multStg1DV[ii]
    
        InputsStg1["engMain"].isp     = InputsNomStg1["engMain"].isp*multStg1Isp[ii]
        
        
        InputsStg2["mPropChillOx"]   = InputsNomStg2["mPropChillOx"]*multStg2Chill[ii]
        InputsStg2["mPropChillFuel"] = InputsNomStg2["mPropChillFuel"]*multStg2Chill[ii]
        
        InputsStg2["mdotPropRCSOx"]   = InputsNomStg2["mdotPropRCSOx"]*multStg2PropRCS[ii]
        InputsStg2["mdotPropRCSFuel"] = InputsNomStg2["mdotPropRCSFuel"]*multStg2PropRCS[ii]
        
        InputsStg2["mdotBoiloffOx"]   = InputsNomStg2["mdotBoiloffOx"]*multStg2Boiloff[ii]
        InputsStg2["mdotBoiloffFuel"] = InputsNomStg2["mdotBoiloffFuel"]*multStg2Boiloff[ii]
        
        InputsStg2["mdotLeakageOx"]   = InputsNomStg2["mdotLeakageOx"]*multStg2Leakage[ii]
        InputsStg2["mdotLeakageFuel"] = InputsNomStg2["mdotLeakageFuel"]*multStg2Leakage[ii]
    
        InputsStg2["pctDVReserveBurned"]   = InputsNomStg2["pctDVReserveBurned"]*multStg1DV[ii]
    
        InputsStg2["engMain"].isp     = InputsNomStg2["engMain"].isp*multStg1Isp[ii]

    # Print some status information
    if ii==int(nCases/4):
        print('25% Complete')
    if ii==int(nCases/2):
        print('50% Complete')
    if ii==int(3*nCases/4):
        print('75% Complete')

    # Run Simulation
    vehicleList = seq.MissionSequence(29751, InputsStg1, 21009, InputsStg2, 1269, 1866)
    
    # Process data
    Stage1 = vehicleList[0]
    Stage2 = vehicleList[1]
    Stage1.Summary()
    Stage2.Summary()
    
    # Save everything to an array for plotting and post processing
    mStg1PropOxVec[ii]  =Stage1.mPropTotalOx
    mStg1PropFuelVec[ii]=Stage1.mPropTotalFuel
    mStg1ExcessVec[ii]  =Stage1.mExcess
    
    mStg2PropOxVec[ii]  =Stage2.mPropTotalOx
    mStg2PropFuelVec[ii]=Stage2.mPropTotalFuel
    mStg2ExcessVec[ii]  =Stage2.mExcess
    
    listStage1.append(copy.deepcopy(Stage1))
    listStage2.append(copy.deepcopy(Stage2))

 
# Sort data in case it's useful for later plotting   
mStg1PropOxVecSort      = np.sort(mStg1PropOxVec[1:].T)   # skip first element since that's a nominal
mStg1PropFuelVecSort    = np.sort(mStg1PropFuelVec[1:].T) # skip first element since that's a nominal
mStg1ExcessVecSort      = np.sort(mStg1ExcessVec[1:].T)   # skip first element since that's a nominal

###########################
# Distribution Plots
###########################    
n, bins, patches = plt.hist(mStg1PropOxVec[1:], bins=40, density=True)
plt.plot(np.array([mStg1PropOxVec[0],mStg1PropOxVec[0]]), np.array([0, np.max(n)]), color='green', linewidth=4)
plt.xlabel('Stage 1 Oxygen Usage (kg)', fontsize=18)
plt.ylabel('Probability', fontsize=18)
plt.xticks(fontsize=16)
plt.grid(True)
plt.show()

n, bins, patches = plt.hist(mStg1PropFuelVec[1:], bins=40, density=True)
plt.plot(np.array([mStg1PropFuelVec[0],mStg1PropFuelVec[0]]), np.array([0, np.max(n)]), color='green', linewidth=4)
plt.xlabel('Stage 1 Fuel Usage (kg)', fontsize=18)
plt.ylabel('Probability', fontsize=18)
plt.grid(True)
plt.show()

n, bins, patches = plt.hist(mStg1ExcessVec[1:], bins=40, density=True)
plt.plot(np.array([mStg1ExcessVec[0],mStg1ExcessVec[0]]), np.array([0, np.max(n)]), color='green', linewidth=4)
plt.xlabel('Stage 1 Excess Mass (kg)', fontsize=18)
plt.ylabel('Probability', fontsize=18)
plt.grid(True)
plt.show()


n, bins, patches = plt.hist(mStg2PropOxVec[1:], bins=40, density=True)
plt.plot(np.array([mStg2PropOxVec[0],mStg2PropOxVec[0]]), np.array([0, np.max(n)]), color='green', linewidth=4)
plt.xlabel('Stage 2 Oxygen Usage (kg)', fontsize=18)
plt.ylabel('Probability', fontsize=18)
plt.grid(True)
plt.show()

n, bins, patches = plt.hist(mStg2PropFuelVec[1:], bins=40, density=True)
plt.plot(np.array([mStg2PropFuelVec[0],mStg2PropFuelVec[0]]), np.array([0, np.max(n)]), color='green', linewidth=4)
plt.xlabel('Stage 2 Fuel Usage (kg)', fontsize=18)
plt.ylabel('Probability', fontsize=18)
plt.grid(True)
plt.show()

n, bins, patches = plt.hist(mStg2ExcessVec[1:], bins=40, density=True)
plt.plot(np.array([mStg2ExcessVec[0],mStg2ExcessVec[0]]), np.array([0, np.max(n)]), color='green', linewidth=4)
plt.xlabel('Stage 2 Excess Mass (kg)', fontsize=18)
plt.ylabel('Probability', fontsize=18)
plt.grid(True)
plt.show()

###########################
# Sensitivity Plots
###########################
m, b = np.polyfit(InputsNomStg1["engMain"].isp*multStg1Isp[1:], mStg1ExcessVec[1:], 1)
print(m)

plt.plot(InputsNomStg1["engMain"].isp*multStg1Isp[1:], mStg1ExcessVec[1:], '.', linewidth=4)
plt.plot(InputsNomStg1["engMain"].isp*multStg1Isp[1:], m*InputsNomStg1["engMain"].isp*multStg1Isp[1:]+b, 'r')
plt.text(450, 300, 'Slope = 34 kg/s', color='red', fontsize=14)
plt.ylabel('Stage 1 Excess Mass (kg)', fontsize=18)
plt.xlabel('Isp (s)', fontsize=18)
plt.grid(True)
plt.show()

m, b = np.polyfit(multStg1Chill[1:]*100, mStg1ExcessVec[1:], 1)
res = stats.linregress(multStg1Chill[1:]*100, mStg1ExcessVec[1:].T)
print(f"R-squared: {res.rvalue**2:.6f}")
plt.plot(multStg1Chill[1:]*100, mStg1ExcessVec[1:], '.', linewidth=4)
plt.plot(multStg1Chill[1:]*100, m*multStg1Chill[1:]*100+b, 'r')
plt.text(80, 300, 'Slope = -2.5 kg/%', color='red', fontsize=14)
plt.ylabel('Stage 1 Excess Mass (kg)', fontsize=18)
plt.xlabel('Chill-In Multiplier (%)', fontsize=18)
plt.grid(True)
plt.show()


m, b = np.polyfit(multStg1Boiloff[1:]*100, mStg1ExcessVec[1:], 1)
res = stats.linregress(multStg1Boiloff[1:]*100, mStg1ExcessVec[1:].T)
print(f"R-squared: {res.rvalue**2:.6f}")
plt.plot(multStg1Boiloff[1:]*100, mStg1ExcessVec[1:], '.', linewidth=4)
plt.plot(multStg1Boiloff[1:]*100, m*multStg1Boiloff[1:]*100+b, 'r')
plt.text(96, 300, 'Slope = -6.2 kg/%', color='red', fontsize=14)
plt.ylabel('Stage 1 Excess Mass (kg)', fontsize=18)
plt.xlabel('Boiloff Multiplier (%)', fontsize=18)
plt.grid(True)
plt.show()