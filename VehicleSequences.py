# -*- coding: utf-8 -*-
"""
Created on Sat Jul 24 09:36:43 2021

@author: jlamp
"""
import numpy as np
import SizerClasses as cf

# Define constants for unit conversions
C_day = 86400
C_hour= 3600
C_s   = 1
C_kg  = 1
C_lbf = 4.448


def MissionSequence(mLaunchStage1, dictStage1In, mLaunchStage2, dictStage2In, dvStage1, mUpmass):
    # A function to calculate propellant and sequence data given certain 
    # vehicle parameters.  
    #
    # Inputs: 
    #   mLaunchStage1:        initial mass of vehicle (kg)
    #   dictStage1In:  dictionary of vehicle parameters
    # Output:
    #    Stage1:         vehicle classes

    # Initialize the vehicle class
    Stage1 = cf.Vehicle(dictStage1In)
    Stage2 = cf.Vehicle(dictStage2In)
    
    # Calculate dV to raise orbit to TLI
    rocketDataApogee = np.array([185, 1000, 5000, 10000, 20000, 30000, 35786, 75000, 100000, 200000, 300000, 410000])
    rocketDataMass   = np.array([38000,34000,28000,22188,20938,19750,19375,15625,14250,14125,14063,14006])*1.5
    
    launchApogeeStg1 = np.interp(mLaunchStage1,rocketDataMass[::-1],rocketDataApogee[::-1]) # the weird -1 reverses the order of the data since interp expects increasing values
    launchApogeeStg2 = np.interp(mLaunchStage2,rocketDataMass[::-1],rocketDataApogee[::-1]) # the weird -1 reverses the order of the data since interp expects increasing values

    dvTLIStg1   = cf.ApogeeRaise(launchApogeeStg1)
    dvTLIStg2   = cf.ApogeeRaise(launchApogeeStg2)

    # Define the sequence of maneuvers and other mass drops
    # For convenience, the phase class has the following inputs
    # (vehVehicle, strName, dtPhase, dvPhase, clsEng, mDrop, strPhaseType, mStartOptional, mAddForDV)
    
    # Stage 1 out to RPOD
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-TLI Settling',  0,       2.5,  dictStage1In["engRCS"],  0,   'Burn',  mLaunchStage1, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'TLI',                0,      dvTLIStg1,  dictStage1In["engMain"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to TCM1',    1.5*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-TCM1 Settling',  0,       2.5,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'TCM1',               0,        20,  dictStage1In["engMain"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to TCM2',    2.0*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'TCM2',               0,         5,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to PLF',     1.0*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-PLF Settling',   0,       2.5,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-PLF Chill',      0,         0,   dictStage1In["engRCS"],  0,   'Chill',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'PLF',                0,       200,  dictStage1In["engMain"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to NRI',     1.0*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-NRI Settling',   0,       2.5,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-NRI Chill',      0,         0,   dictStage1In["engRCS"],  0,   'Chill',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'NRI',                0,       225,  dictStage1In["engMain"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to SK1',     6.5*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'SK1',                0,         1,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to SK2',     6.5*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'SK2',                0,         1,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to SK3',     6.5*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'SK3',                0,       0.5,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to SK4',     6.5*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'SK4',                0,       0.5,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to SK5',     6.5*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'SK5',                0,       0.5,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to SK6',     6.5*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, 0) )
    Stage1.phaseList.append( cf.Phase(Stage1,'SK6',                0,       0.5,   dictStage1In["engRCS"],  0,   'Burn',        0, 0) )
    
    ######################
    # Stage 2 out to RPOD
    ######################
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-TLI Settling',  0,       2.5,  dictStage2In["engRCS"],  0,   'Burn',  mLaunchStage2, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'TLI',                0,      dvTLIStg2,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Coast to TCM1',    1.5*C_day,   0,  dictStage2In["engRCS"],  0,  'Coast',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-TCM1 Settling',  0,       2.5,   dictStage2In["engRCS"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'TCM1',               0,        20,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Coast to TCM2',    2.0*C_day,   0,  dictStage2In["engRCS"],  0,  'Coast',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'TCM2',               0,         5,   dictStage2In["engRCS"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Coast to PLF',     1.0*C_day,   0,  dictStage2In["engRCS"],  0,  'Coast',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-PLF Settling',   0,       2.5,   dictStage2In["engRCS"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-PLF Chill',      0,         0,   dictStage2In["engRCS"],  0,   'Chill',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'PLF',                0,       200,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Coast to NRI',     1.0*C_day,   0,  dictStage2In["engRCS"],  0,  'Coast',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-NRI Settling',   0,       2.5,   dictStage2In["engRCS"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-NRI Chill',      0,         0,   dictStage2In["engRCS"],  0,   'Chill',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'NRI',                0,       225,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    
    mStage2atStage1Pickup = Stage2.phaseList[14].mEnd

    
    ###############################
    # Stage 1 Picks up Stage 2 Mass
    ###############################
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to NRD',     1.0*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-NRD Settling',   0,       2.5,   dictStage1In["engRCS"],  0,   'Burn',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-NRD Chill',      0,         0,   dictStage1In["engRCS"],  0,   'Chill',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'NRD',                0,        80,  dictStage1In["engMain"],  0,   'Burn',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to LOI',     0.5*C_day,   0,  dictStage1In["engRCS"],  0,  'Coast',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-LOI Settling',   0,       2.5,   dictStage1In["engRCS"],  0,   'Burn',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-LOI Chill',      0,         0,   dictStage1In["engRCS"],  0,   'Chill',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'LOI',                0,       650,  dictStage1In["engMain"],  0,   'Burn',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to DOI',     4*C_hour,    0,  dictStage1In["engRCS"],  0,  'Coast',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-DOI Settling',   0,       2.5,   dictStage1In["engRCS"],  0,   'Burn',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-DOI Chill',      0,         0,   dictStage1In["engRCS"],  0,   'Chill',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'DOI',                0,      18.5,  dictStage1In["engMain"],  0,   'Burn',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Coast to PDI',     4*C_hour,    0,  dictStage1In["engRCS"],  0,  'Coast',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-PDI Settling',   0,       2.5,   dictStage1In["engRCS"],  0,   'Burn',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'Pre-PDI Chill',      0,         0,   dictStage1In["engRCS"],  0,   'Chill',        0, mStage2atStage1Pickup) )
    Stage1.phaseList.append( cf.Phase(Stage1,'PDI',                0,      dvStage1,  dictStage1In["engMain"],  0,   'Burn',        0, mStage2atStage1Pickup) )
    

    ################################
    # Stage 2 after Stage 1 Jettison
    ################################
    Stage2.phaseList.append( cf.Phase(Stage2,'PDI',                0,      1985-dvStage1,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Surface Time',    6.5*C_day,    0,  dictStage2In["engRCS"],  0,  'Coast',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pickup Samples',     0,         0,   dictStage2In["engRCS"], -mUpmass,   'Coast',        0, 0) )

    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-Asc Chill',      0,         0,   dictStage2In["engRCS"],  0,   'Chill',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Ascent',             0,      1800,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Coast to Circ',   1*C_hour,     0,  dictStage2In["engRCS"],  0,  'Coast',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-Circ Chill',     0,         0,   dictStage2In["engRCS"],  0,   'Chill',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Circ',               0,        20,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Coast to LLOD',     4*C_hour,   0,  dictStage2In["engRCS"],  0,  'Coast',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-LLOD Settling',   0,      2.5,   dictStage2In["engRCS"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-LLOD Chill',      0,        0,   dictStage2In["engRCS"],  0,   'Chill',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'LLOD',                0,      670,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Coast to NRI',     12*C_hour,   0,  dictStage2In["engRCS"],  0,  'Coast',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-NRI Settling',   0,       2.5,   dictStage2In["engRCS"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Pre-NRI Chill',      0,         0,   dictStage2In["engRCS"],  0,   'Chill',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'NRI',                0,        90,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    Stage2.phaseList.append( cf.Phase(Stage2,'Offload Samples',    0,         0,  dictStage2In["engMain"],  mUpmass,   'Coast',        0, 0) )

    Stage2.phaseList.append( cf.Phase(Stage2,'End of Mission',     0,         0,  dictStage2In["engMain"],  0,   'Burn',        0, 0) )
    return [Stage1, Stage2]