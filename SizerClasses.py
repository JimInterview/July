

#####################################################
# This file contains a variety of classes and functions
# necessary for the rest of the codebase
#####################################################



import numpy as np

def ApogeeRaise(apogeeStart):
    # Inputs:
    #   apogeeStart: initial orbit apogee altitude (km)
    # Outputs:
    #   delta-v needed to reach 410,000 km apogee altitude (m/s)
    mu = 398600
    apogeeEnd = 410000
    rEarth = 6378
    rStart = rEarth+185
    aStart = (rEarth+185+rEarth+apogeeStart)/2
    aEnd   = (rEarth+185+rEarth+apogeeEnd)/2
        
    vStart = np.sqrt(mu*(2/rStart-1/aStart))
    vEnd   = np.sqrt(mu*(2/rStart-1/aEnd))
    dv     = vEnd-vStart
       
    return dv*1000 # multiply by 1000 to put in m/s


class Vehicle:
    # This class contains information about the vehicle, such as fluid usage 
    # parameters, overall propellant, and what engines are used.
    def __init__(self, dictIn):
        
        self.strName            = dictIn["strName"]
        self.engMain            = dictIn["engMain"] 
        self.engRCS             = dictIn["engRCS"]
        self.mPropChillOx       = dictIn["mPropChillOx"]
        self.mPropChillFuel     = dictIn["mPropChillFuel"]
        self.mdotPropRCSOx      = dictIn["mdotPropRCSOx"]
        self.mdotPropRCSFuel    = dictIn["mdotPropRCSFuel"]
        self.mdotBoiloffOx      = dictIn["mdotBoiloffOx"]
        self.mdotBoiloffFuel    = dictIn["mdotBoiloffFuel"]
        self.mdotLeakageOx      = dictIn["mdotLeakageOx"]
        self.mdotLeakageFuel    = dictIn["mdotLeakageFuel"]
        self.mPropVentOx        = dictIn["mPropVentOx"]
        self.mPropVentFuel      = dictIn["mPropVentFuel"]
        self.pctDVReserveBurned = dictIn["pctDVReserveBurned"]
        self.pctResidualOx      = dictIn["pctResidualOx"]
        self.pctResidualFuel    = dictIn["pctResidualFuel"]
        self.mDryAllocation     = dictIn["mDryAllocation"] 
        self.mMaxOxLoad         = dictIn["mMaxOxLoad"]
        self.mMaxFuelLoad       = dictIn["mMaxFuelLoad"]
        self.coeffOxMass        = dictIn["coeffOxMass"]
        self.coeffFuelMass      = dictIn["coeffFuelMass"]
        self.coeffFixedMass     = dictIn["coeffFixedMass"]
        self.phaseList          = []
    
    def Summary(self):
        # This function sums up all propellant usages across phases and types 
        # and categorizes the final masses
        mPropImpulse     = 0
        mPropImpulseOx   = 0
        mPropImpulseFuel = 0
        
        mPropImpulseReserve = 0
        mPropImpulseReserveOx = 0
        mPropImpulseReserveFuel = 0
        
        mPropBoiloff     = 0
        mPropBoiloffOx   = 0
        mPropBoiloffFuel = 0
       
        mPropLeakage     = 0
        mPropLeakageOx   = 0
        mPropLeakageFuel = 0 
       
        mPropRCS         = 0 
        mPropRCSOx       = 0
        mPropRCSFuel     = 0
        
        mPropChill     = 0
        mPropChillOx   = 0
        mPropChillFuel = 0
        
        mPropVent     = 0
        mPropVentOx   = 0
        mPropVentFuel = 0 
        
        mPropConsumedSequentialOx   = np.zeros((np.size(self.phaseList), 1))
        mPropConsumedSequentialFuel = np.zeros((np.size(self.phaseList), 1))
        indCounter =0
        for curPhase in self.phaseList:
            mPropImpulse     += curPhase.mPropImpulse 
            mPropImpulseOx   += curPhase.mPropImpulseOx
            mPropImpulseFuel += curPhase.mPropImpulseFuel
            
            mPropImpulseReserve +=  curPhase.mPropImpulseReserve
            mPropImpulseReserveOx +=  curPhase.mPropImpulseReserveOx
            mPropImpulseReserveFuel += curPhase.mPropImpulseReserveFuel
            
            mPropBoiloff     +=  curPhase.mPropBoiloff
            mPropBoiloffOx   +=  curPhase.mPropBoiloffOx
            mPropBoiloffFuel +=  curPhase.mPropBoiloffFuel
            
            mPropLeakage         +=  curPhase.mPropLeakage 
            mPropLeakageOx       +=  curPhase.mPropLeakageOx 
            mPropLeakageFuel     +=  curPhase.mPropLeakageFuel
                        
            mPropRCS         +=  curPhase.mPropRCS 
            mPropRCSOx       +=  curPhase.mPropRCSOx 
            mPropRCSFuel     +=  curPhase.mPropRCSFuel 
            
            mPropChill       += curPhase.mPropChill
            mPropChillOx     += curPhase.mPropChillOx
            mPropChillFuel   += curPhase.mPropChillFuel

            mPropVent       += curPhase.mPropVent
            mPropVentOx     += curPhase.mPropVentOx
            mPropVentFuel   += curPhase.mPropVentFuel            

            mPropConsumedSequentialOx[indCounter]   = mPropImpulseOx   + mPropBoiloffOx   + mPropLeakageOx   + mPropRCSOx + mPropChillOx + mPropVentOx
            mPropConsumedSequentialFuel[indCounter] = mPropImpulseFuel + mPropBoiloffFuel + mPropLeakageFuel + mPropRCSFuel + mPropChillFuel + mPropVentFuel
            indCounter +=1

            
        mPropConsumedOx   = mPropImpulseOx   + mPropBoiloffOx   + mPropLeakageOx   + mPropRCSOx + mPropChillOx + mPropVentOx
        mPropConsumedFuel = mPropImpulseFuel + mPropBoiloffFuel + mPropLeakageFuel + mPropRCSFuel + mPropChillFuel + mPropVentFuel
        mPropConsumedTotal = mPropConsumedOx + mPropConsumedFuel 
        
        mPropResidualOx    = self.pctResidualOx*(mPropConsumedOx+mPropImpulseReserveOx)
        mPropResidualFuel  = self.pctResidualFuel*(mPropConsumedFuel+mPropImpulseReserveFuel)
        mPropResidualTotal = mPropResidualOx + mPropResidualFuel 
        
        mPropAtLandingOx    = mPropResidualOx   + mPropImpulseReserveOx
        mPropAtLandingFuel  = mPropResidualFuel + mPropImpulseReserveFuel
        mPropAtLandingTotal = mPropAtLandingOx + mPropAtLandingFuel 
        
        mPropTotalOx = mPropConsumedOx + mPropAtLandingOx
        mPropTotalFuel = mPropConsumedFuel + mPropAtLandingFuel
        mPropTotalTotal = mPropTotalOx + mPropTotalFuel 
        
        

        if self.mDryAllocation<0:
            # This is a flag that we're using a mass estimating relationship
            self.mDryAllocation = self.coeffFixedMass + self.coeffOxMass*mPropTotalOx + self.coeffFuelMass*mPropTotalFuel
            
  
        mExcess = self.phaseList[-1].mEnd-mPropAtLandingTotal - self.mDryAllocation
        
        self.mPropImpulse      = mPropImpulse
        self.mPropImpulseOx    = mPropImpulseOx
        self.mPropImpulseFuel  = mPropImpulseFuel

        self.mPropImpulseReserve = mPropImpulseReserve
        self.mPropImpulseReserveOx = mPropImpulseReserveOx
        self.mPropImpulseReserveFuel = mPropImpulseReserveFuel

        self.mPropBoiloff       = mPropBoiloff
        self.mPropBoiloffOx     = mPropBoiloffOx
        self.mPropBoiloffFuel   = mPropBoiloffFuel
        
        self.mPropLeakage       = mPropLeakage
        self.mPropLeakageOx     = mPropLeakageOx
        self.mPropLeakageFuel   = mPropLeakageFuel
        
        self.mPropRCS           = mPropRCS
        self.mPropRCSOx         = mPropRCSOx
        self.mPropRCSFuel       = mPropRCSFuel
        
        self.mPropChill         = mPropChill
        self.mPropChillOx       = mPropChillOx
        self.mPropChillFuel     = mPropChillFuel

        self.mPropVent          = mPropVent
        self.mPropVentOx        = mPropVentOx
        self.mPropVentFuel      = mPropVentFuel
        
        self.mPropConsumedSequentialOx = mPropConsumedSequentialOx
        self.mPropConsumedSequentialFuel = mPropConsumedSequentialFuel


        self.mPropConsumedOx    = mPropConsumedOx
        self.mPropConsumedFuel  = mPropConsumedFuel
        self.mPropConsumedTotal = mPropConsumedTotal
        
        self.mPropResidualOx    = mPropResidualOx
        self.mPropResidualFuel  = mPropResidualFuel
        self.mPropResidualTotal = mPropResidualTotal
        
        self.mPropAtLandingOx   = mPropAtLandingOx
        self.mPropAtLandingFuel = mPropAtLandingFuel
        self.mPropAtLandingTotal= mPropAtLandingTotal
        
        self.mPropTotalOx       = mPropTotalOx
        self.mPropTotalFuel     = mPropTotalFuel
        self.mPropTotalTotal    = mPropTotalTotal

        self.mExcess            = mExcess
        
    def __str__(self):
        # This function prints out information about the class
        print('{0:25s}'.format("-------------------------------------------------------------------------------------------------" ))
        print('{0:20s}{1:>13s}{2:>13s}{3:>13s}{4:>13s}{5:>13s}{6:>13s}'.format("Phase Name","Length (s)", "DV (m/s)", "Mass0 (kg)", "MassF (kg)", "OxMass(kg)", "FuelMass(kg)" ))
        print('{0:25s}'.format("-------------------------------------------------------------------------------------------------" ))
        for curPhase in self.phaseList:
            print('{0:20s}{1:13s}{2:13.1f}{3:13.1f}{4:13.1f}{5:13.1f}{6:13.1f}'.format(curPhase.strName,convertTime(curPhase.dtPhase), curPhase.dvPhase, curPhase.mStart, curPhase.mEnd, curPhase.mPropTotalOx, curPhase.mPropTotalFuel ))
        print('')
        print('{0:20s}'.format("------------------------------------------------------------------------" ))
        print('{0:20s}{1:11.1f}'.format("Start Mass (kg): ", self.phaseList[0].mStart))
        print('{0:20s}{1:11.1f}'.format("Final Mass (kg): ", self.phaseList[-1].mEnd))
        print('{0:20s}{1:11.1f}'.format("Dry Mass (kg): ", self.mDryAllocation))                              
        print('{0:20s}{1:11.1f}'.format("Reserve Prop (kg): ", self.mPropImpulseReserve))    
        print('{0:20s}{1:11.1f}'.format("Residual Prop (kg): ", self.mPropResidualTotal))  
        print('{0:20s}{1:11.1f}'.format("Mass Check (kg): ", self.mDryAllocation+self.mPropResidualTotal+self.mPropImpulseReserve))  
        print('{0:20s}{1:11.1f}'.format("Avail Mass (kg): ", self.mExcess))        
        print('{0:20s}'.format("------------------------------------------------------------------------" ))
        print('{0:20s}{1:11.1f}'.format("Ox Mass (kg): ", self.mPropTotalOx))
        print('{0:20s}{1:11.1f}'.format("Fuel Mass (kg): ", self.mPropTotalFuel))          
        return ""
            

class Phase:
    # This class contains information about an indivudal phase: dV, prop usage
    # type, etc
    def __init__(self,vehVehicle, strName, dtPhase, dvPhase, clsEng, mDrop, strPhaseType, mStartOptional, mAddForDV):

        prevEvent = np.size(vehVehicle.phaseList)        

        if prevEvent==0:
            dtStart = 0
            mStart = mStartOptional
            mStartForDV = mStartOptional
        else:
            dtStart = vehVehicle.phaseList[prevEvent-1].dtEnd    
            mStart  = vehVehicle.phaseList[prevEvent-1].mEnd
            mStartForDV = mStart+mAddForDV
            
        # Modify phase duration if this is a burn 
        if dvPhase>0:
            mPropImpulse     =  mStartForDV - mStartForDV / np.exp(dvPhase/(9.80665*clsEng.isp))
            mPropImpulseReserve =  mStartForDV - mStartForDV / np.exp(dvPhase*(1+vehVehicle.pctDVReserveBurned)/(9.80665*clsEng.isp))-mPropImpulse
            dtPhase             =  mPropImpulse/clsEng.mdot

        else:
            mPropImpulse        = 0
            mPropImpulseReserve = 0
        
            
        mPropImpulseOx          = mPropImpulse*clsEng.mr/(1+clsEng.mr)
        mPropImpulseFuel        = mPropImpulse/(1+clsEng.mr)
            
        mPropImpulseReserveOx   = mPropImpulseReserve*clsEng.mr/(1+clsEng.mr)
        mPropImpulseReserveFuel = mPropImpulseReserve/(1+clsEng.mr)
     

    

        
        # Determine boiloff, leakage, and RCS losses
        mPropBoiloffOx      = vehVehicle.mdotBoiloffOx*dtPhase
        mPropBoiloffFuel    = vehVehicle.mdotBoiloffFuel*dtPhase
        mPropBoiloff        = mPropBoiloffOx + mPropBoiloffFuel
        
        mPropLeakageOx      = vehVehicle.mdotLeakageOx*dtPhase
        mPropLeakageFuel    = vehVehicle.mdotLeakageFuel*dtPhase
        mPropLeakage        = mPropLeakageOx + mPropLeakageFuel
        

        mPropRCSOx      = vehVehicle.mdotPropRCSOx*dtPhase
        mPropRCSFuel    = vehVehicle.mdotPropRCSFuel*dtPhase
        mPropRCS        = mPropRCSOx + mPropRCSFuel
        
        # Check if this is a chill-in phase
        if strPhaseType =="Chill":
          
            mPropChillOx    = vehVehicle.mPropChillOx
            mPropChillFuel  = vehVehicle.mPropChillFuel
            mPropChill      = mPropChillOx + mPropChillFuel
        else:
            mPropChillOx    = 0
            mPropChillFuel  = 0
            mPropChill      = 0
            
        # Check if this is a vent phase
        if strPhaseType =="Vent":
          
            mPropVentOx    = vehVehicle.mPropVentOx
            mPropVentFuel  = vehVehicle.mPropVentFuel
            mPropVent      = mPropVentOx + mPropVentFuel
        else:
            mPropVentOx    = 0
            mPropVentFuel  = 0
            mPropVent      = 0
        
        mPropTotalOx = mPropImpulseOx+mPropBoiloffOx+mPropLeakageOx+mPropRCSOx+mPropChillOx+mPropVentOx
        mPropTotalFuel = mPropImpulseFuel+mPropBoiloffFuel+mPropLeakageFuel+mPropRCSFuel+mPropChillFuel+mPropVentFuel
   
        
        
        # Determine final time and final mass        
        dtEnd = dtStart+dtPhase
        mEnd  = mStart - mPropImpulse - mPropBoiloff - mPropLeakage - mPropRCS - mPropChill - mPropVent - mDrop
       
        
        # Move data to class structure to save information
        self.dtStart        = dtStart
        self.mStart         = mStart
        self.mStartForDV    = mStartForDV
        self.mPropImpulse   = mPropImpulse
        self.mPropImpulseOx = mPropImpulseOx
        self.mPropImpulseFuel = mPropImpulseFuel
        self.mPropImpulseReserve = mPropImpulseReserve
        self.mPropImpulseReserveOx = mPropImpulseReserveOx
        self.mPropImpulseReserveFuel = mPropImpulseReserveFuel
        self.mPropBoiloff       = mPropBoiloff
        self.mPropBoiloffOx     = mPropBoiloffOx
        self.mPropBoiloffFuel   = mPropBoiloffFuel
        self.mPropLeakage       = mPropLeakage
        self.mPropLeakageOx     = mPropLeakageOx
        self.mPropLeakageFuel   = mPropLeakageFuel
        self.mPropRCS       = mPropRCS
        self.mPropRCSOx     = mPropRCSOx
        self.mPropRCSFuel   = mPropRCSFuel
        self.mPropLeakage       = mPropLeakage
        self.mPropLeakageOx     = mPropLeakageOx
        self.mPropLeakageFuel   = mPropLeakageFuel
        self.mPropChill             = mPropChill
        self.mPropChillOx           = mPropChillOx
        self.mPropChillFuel         = mPropChillFuel
        self.mPropVent             = mPropVent
        self.mPropVentOx           = mPropVentOx
        self.mPropVentFuel         = mPropVentFuel
        self.mPropTotalOx          = mPropTotalOx
        self.mPropTotalFuel        = mPropTotalFuel
        
        
        
        self.mEnd           = mEnd
        self.dtPhase        = dtPhase
        self.dtEnd          = dtEnd
        self.dvPhase        = dvPhase
        self.clsEng         = clsEng
        self.strName        = strName
   



class Engine:
    # This class defines an engine
    def __init__(self,isp, thrust, mr):
        self.mr = mr
        self.isp = isp
        self.thrust = thrust
        self.mdot = thrust/isp/9.80665



def convertTime(secs):
    # This function converts seconds to a days:hours:minutes:seconds format
    days = secs//86400
    hours = (secs - days*86400)//3600
    minutes = (secs - days*86400 - hours*3600)//60
    seconds = secs - days*86400 - hours*3600 - minutes*60
    result = ("{0:02.0f}:".format(days) if days else "00:") + \
    ("{0:02.0f}:".format(hours) if hours else "00:") + \
    ("{0:02.0f}:".format(minutes) if minutes else "00:") + \
    ("{0:02.0f}".format(seconds) if seconds else "00")
    return result

