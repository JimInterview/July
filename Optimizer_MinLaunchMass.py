
#####################################################
# This file runs an optimization case 
# minimizing the sum of launch masses of two vehicles
#####################################################



# Import necessary classes
import numpy as np            # Numerical package
from scipy import optimize    # Optimization package

import VehicleSequences as seq # Custom file with sequence information
import VehicleInputs as inp    # Contains standard vehicle inputs



# Initialize a variable to save optimization iterations (not best practice, 
# but useful for debugging)
iterationHolder = list()



#########################################################################
# Begin Definition of Constraints and Objective
#########################################################################
def launchMinimizer(optVec):
    # The objective function to be used by the optimizer.  In this case, we 
    # are minimizing the sum of the launch mass of the two vehicles
    # 
    # Inputs:
    #   optVec[0]: launch mass of stage 1 (kg)
    #   optVec[1]: launch mass of stage 2 (kg)
    #   optVec[2]: delta-v performed during stage split by stage 1 (m/s)
    # Outputs:
    #   sum of the two launch masses
    mLaunchStg1 = optVec[0]
    mLaunchStg2 = optVec[1]
    iterationHolder.append(optVec)
    return mLaunchStg1+mLaunchStg2


def excessMassConstraintStage1(optVec, InputsStg1, InputsStg2, mTargetUpMass):
    # A function to be used as a constraint by the optimizer.  We're constraining
    # "mExcess" to be equal than zero.  mExcess represents the difference between 
    # the final mass and any residual/reserve/dry mass (so zero means that the 
    # final mass is equal to residual+reserve+dry).  
    #
    # In this case, we're calling the MissionSequence each time to calculate
    # the vehicle parameters.  
    #
    # Inputs:
    #   optVec[0]: launch mass of stage 1 (kg)
    #   optVec[1]: launch mass of stage 2 (kg)
    #   optVec[2]: delta-v performed during stage split by stage 1 (m/s)
    # Outputs:
    #   mExcess for Stage 1 (kg)
   
    vehicleList = seq.MissionSequence(optVec[0], InputsStg1, optVec[1], InputsStg2, optVec[2], mTargetUpMass)
    Stage1 = vehicleList[0]
    Stage2 = vehicleList[1]
    
    Stage1.Summary()
    Stage2.Summary()
    return Stage1.mExcess  
 
def excessMassConstraintStage2(optVec, InputsStg1, InputsStg2, mTargetUpMass):
    # A function to be used as a constraint by the optimizer.  We're constraining
    # "mExcess" to be equal than zero.  mExcess represents the difference between 
    # the final mass and any residual/reserve/dry mass (so zero means that the 
    # final mass is equal to residual+reserve+dry).  
    #
    # In this case, we're calling the MissionSequence each time to calculate
    # the vehicle parameters.  
    #
    # Inputs:
    #   optVec[0]: launch mass of stage 1 (kg)
    #   optVec[1]: launch mass of stage 2 (kg)
    #   optVec[2]: delta-v performed during stage split by stage 1 (m/s)
    # Outputs:
    #   mExcess for Stage 2 (kg)
    vehicleList = seq.MissionSequence(optVec[0], InputsStg1, optVec[1], InputsStg2, optVec[2], mTargetUpMass)
    Stage1 = vehicleList[0]
    Stage2 = vehicleList[1]
    
    Stage1.Summary()
    Stage2.Summary()
    return Stage2.mExcess


########################################################
# Begin optimizer information
########################################################
# Since we're minimizing launch mass, fix the target upmass
mTargetUpMass = 1200

# Link in the constraint functions 
constDict = ({"fun": excessMassConstraintStage1, "type": "eq", "args":(inp.InputsStg1, inp.InputsStg2, mTargetUpMass)},
             {"fun": excessMassConstraintStage2, "type": "eq", "args":(inp.InputsStg1, inp.InputsStg2, mTargetUpMass)})

# Initial guesses for optimizer
mLaunchStg1Guess = 22500
mLaunchStg2Guess = 21000
dvSplitStg1Guess = 1000



# Actually run the optimizer
soln = optimize.minimize(launchMinimizer, np.array([[mLaunchStg1Guess], [mLaunchStg2Guess], [dvSplitStg1Guess]]), method="SLSQP", #bounds=((14006, 38000),(14006, 38000)),
                     constraints=constDict, tol=1e-4, options={'disp': True})


########################################################
# Run the optimized version through the Mission Sequence
########################################################

vehicleList = seq.MissionSequence(soln.x[0], inp.InputsStg1, soln.x[1], inp.InputsStg2, soln.x[2], mTargetUpMass)
Stage1 = vehicleList[0]
Stage2 = vehicleList[1]
 
Stage1.Summary()
Stage2.Summary()


# The lines below are useful for debugging optimizer performance
# # Initialize some variables and then print optimizer performance to verify 
# # converging tendencies
# stored = np.zeros(np.size(iterationHolder,0))
# stored2=np.zeros(np.size(iterationHolder,0))
# print('{0:25s}'.format("----------------------------------------" ))
# print('{0:>5s}{1:>13s}{2:>15s}'.format("Iter", "OptVar (kg)",  "Constrnt (kg)"))
# print('{0:25s}'.format("----------------------------------------" ))
# for ind,val in enumerate(stored):
#     stored[ind]=launchMinimizer(iterationHolder[ind])
#     stored2[ind]=excessMassConstraintStage1(iterationHolder[ind], inp.InputsStg1, inp.InputsStg2, mTargetUpMass)
#     print('{0:5d}{1:13.3f}{2:15.3f}'.format(ind, stored[ind], stored2[ind]))






