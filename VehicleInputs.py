
#####################################################
# This file contains inputs for two vehicles, including
# propulsion parameters and mass relationships
#####################################################



# Define constants for unit conversions
C_day = 86400
C_hour= 3600
C_s   = 1
C_kg  = 1
C_lbf = 4.448

import SizerClasses as cf


# Stage 1 Definitions
nMainEnginesStg1=3
engMainStg1 = cf.Engine(450, 10000*C_lbf*nMainEnginesStg1, 5.5)
engRCSStg1  = cf.Engine(400, 100,  3.5)
 
InputsStg1 = {"strName":        "Stage 1",
   "engMain":        engMainStg1,
   "engRCS":         engRCSStg1,
   "mPropChillOx":          15*nMainEnginesStg1, 
   "mPropChillFuel":        15*nMainEnginesStg1,
   "mdotPropRCSOx":  5*C_kg/C_day,
   "mdotPropRCSFuel":1*C_kg/C_day,
   "mdotBoiloffOx":  10*C_kg/C_day,
   "mdotBoiloffFuel":10*C_kg/C_day,
   "mdotLeakageOx":  1*C_kg/C_day,
   "mdotLeakageFuel":1*C_kg/C_day,
   "mPropVentOx":    10, 
   "mPropVentFuel":  10,
   "pctDVReserveBurned": 0.015, 
   "pctResidualOx":      0.015, 
   "pctResidualFuel":    0.015,
   "mDryAllocation":      -5000, # negative means use the coefficients
   "mMaxOxLoad":      18000, 
   "mMaxFuelLoad":    3500,
   "coeffOxMass":      0.05,
   "coeffFuelMass":    0.08,
   "coeffFixedMass":   4200}

# Stage 2 Definitions
nMainEnginesStg2=3
engMainStg2 = cf.Engine(340, 3750*C_lbf*nMainEnginesStg2, 1.8)
engRCSStg2  = cf.Engine(220, 100,  1.8)
 

InputsStg2 = {"strName":        "Stage 2",
   "engMain":        engMainStg2,
   "engRCS":         engRCSStg2,
   "mPropChillOx":          0*nMainEnginesStg2, 
   "mPropChillFuel":        0*nMainEnginesStg2,
   "mdotPropRCSOx":  3*C_kg/C_day,
   "mdotPropRCSFuel":1*C_kg/C_day,
   "mdotBoiloffOx":  0*C_kg/C_day,
   "mdotBoiloffFuel":0*C_kg/C_day,
   "mdotLeakageOx":  1*C_kg/C_day,
   "mdotLeakageFuel":1*C_kg/C_day,
   "mPropVentOx":    0, 
   "mPropVentFuel":  0,
   "pctDVReserveBurned": 0.015, 
   "pctResidualOx":      0.015, 
   "pctResidualFuel":    0.015,
   "mDryAllocation":      -5000, # negative means use the coefficients
   "mMaxOxLoad":      10000, 
   "mMaxFuelLoad":    10000, 
   "coeffOxMass":      0.01,
   "coeffFuelMass":    0.02,
   "coeffFixedMass":   5100}